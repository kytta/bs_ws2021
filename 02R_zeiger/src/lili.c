#include "lili.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

element_t* head = NULL;

unsigned int* insert_element(unsigned int value)
{
    element_t* new_element = (element_t*)malloc(sizeof(element_t));
    if (new_element == NULL)
    {
        perror("Allocation faulty");
        unsigned int* err = NULL;
        return err;
    }

    if (head == NULL)
    {
        head       = new_element;
        head->next = NULL;
    }
    else
    {
        element_t* last = head;
        while (last->next != NULL)
        {
            last = last->next;
        }
        last->next = new_element;
    }

    new_element->data = value;
    new_element->next = NULL;

    return &(new_element->data);
}

unsigned int remove_element(void)
{
    unsigned int value;
    if (head != NULL)
    {
        element_t* new_head = head->next;

        value = head->data;
        free(head);
        head = new_head;
        return value;
    }
    else
    {
        printf("WARNING: nothing to remove, lili is empty");
        return 0;
    }
}

void print_lili(void)
{
    printf("print_lili: ");

    if (head != NULL)
    {
        element_t* last = head;

        do
        {
            printf("%d, ", last->data);
            last = last->next;
        }
        while (last != NULL);
    }

    printf("\n");
    return;
}
