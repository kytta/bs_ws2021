#include "mymalloc.h"

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "colorCodes.h"
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

chunk_t* root = NULL;

char checkpoint_name[256];
int checkpoint_counter = 0;

//============================================================================//

//! --------------------------------
//! Write your helper functions here
//! --------------------------------

//============================================================================//

/**
 * @brief get_page_size returns page size in bytes
 * @return page size in byte
 */
size_t get_page_size(void) { return sysconf(_SC_PAGESIZE); }

//----------------------------------------------------------------------------//

/**
 * @brief open_file: opens a file with the size of 1 page for that we want to
 * manage the memory
 * @return file descriptor
 */
int open_file(void)
{
    int fd = open(FILE_NAME, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    if (fd == -1) // open returns -1 on error
    {
        perror("open_file: open");
        exit(1);
    }

    int j = posix_fallocate(fd, 0, get_page_size());
    if (j != 0) // posix_fallocate returns error code on error
    {
        errno = j;
        perror("open_file: posix_fallocate");
        exit(1);
    }

    return fd;
}

//----------------------------------------------------------------------------//

/**
 * @brief my_malloc allocates memory that is backed by a file
 * @param size: size of memory to allocate in bytes
 * @return pointer to allocated memory
 */
void* my_malloc(size_t size)
{
    // 1
    if (root == NULL)
    {
        int file = open_file();

        // bind file to address space
        root = mmap(START_ADDRESS, get_page_size(), PROT_READ | PROT_WRITE,
                    MAP_FIXED | MAP_SHARED, file, 0);
        if (root == MAP_FAILED) // mmap returns MAP_FAILED (-1) on error
        {
            perror("my_malloc: mmap");
            exit(1);
        }

        root->file = file;
        root->size = get_page_size();

        memory_block_t* block = (memory_block_t*)root + 1;
        root->free_space_list = block;

        root->free_space_list->size =
            get_page_size() - sizeof(chunk_t) - sizeof(memory_block_t);
        root->free_space_list->next     = NULL;
        root->free_space_list->previous = NULL;
    }

    // 2
    // size in bytes as a ceiled multiple of memory_block_t
    size_t needed_size =
        (((size - 1) / sizeof(memory_block_t)) + 1) * sizeof(memory_block_t);
    // 3
    memory_block_t* save_to = root->free_space_list;
    while (save_to->size < needed_size)
    {
        save_to = save_to->next;

        if (save_to == NULL) // there is no contiguous free memory area
        {
            errno = ENOMEM;
            return NULL;
        }
    }

    // 4
    // memory left in block after allocation
    size_t mem_left = save_to->size - needed_size;

    if (mem_left <= sizeof(memory_block_t)) // no place after allocation
    {
        needed_size = save_to->size;
        if (save_to->previous == NULL) // if this is the first block
        {
            if (save_to->next == NULL) // and it's the last block as well
            {
                root->free_space_list = NULL; // we have no free space left
            }
            else // our next block will become the first free block
            {
                root->free_space_list   = save_to->next;
                save_to->next->previous = NULL;
            }
        }
        else // our current block is somewhere in the middle but cant birth a
             // new smaller block
        {
            save_to->previous->next =
                save_to->next; // so our list has to skip it
            if (save_to->next != NULL)
            {
                save_to->next->previous = save_to->previous;
            }
        }
    }
    else // we can split the current block
    {
        memory_block_t* newBlock =
            save_to + 1 + (needed_size / sizeof(memory_block_t));

        newBlock->size = mem_left - sizeof(memory_block_t);

        // there was a free block before the current one
        if (save_to->previous != NULL)
        {
            save_to->previous->next = newBlock; // this memoryblock
        }
        else // this block is the first block
        {
            root->free_space_list = newBlock;
        }
        newBlock->previous = save_to->previous;

        // there is a free block after the current one
        if (save_to->next != NULL)
        {
            save_to->next->previous = newBlock;
        }
        newBlock->next = save_to->next;
    }

    save_to->next     = MAGIC_NUMBER;
    save_to->previous = MAGIC_NUMBER;
    save_to->size     = needed_size;

    snprintf(checkpoint_name, 256, "%03d_my_malloc", checkpoint_counter++);
    checkpoint(checkpoint_name);

    return (void*)(save_to + 1); // return pointer to memory
}

//----------------------------------------------------------------------------//

/**
 * @brief my_free: releases memory that was allocated by my_free
 * @param memory_location: pointer to memory that should be freed
 */
void my_free(void* memory_location)
{
    if (memory_location == NULL)
    {
        return;
    }

    memory_block_t* block = (memory_block_t*)memory_location - 1;

    // root is NULL
    // or no MAGIC_NUMBERs present
    if (root == NULL || block->next != MAGIC_NUMBER ||
        block->previous != MAGIC_NUMBER)
    {
        fprintf(stderr, "No memory was allocated\n");
        return;
    }

    if (root->free_space_list == NULL) // if our list currently has no free
                                       // space
    {
        root->free_space_list = block;
        block->next           = NULL;
        block->previous       = NULL;
    }

    if (block < root->free_space_list)
    {
        // merge case 2.1
        // block in front of first free

        memory_block_t* t     = root->free_space_list;
        root->free_space_list = block;
        block->next           = t;
        t->previous           = block;
        block->previous       = NULL;

        // if the next free block lies adjacent to the freed one
        // we can merge these together
        //
        // pseudocode:
        // if (t == *block + sizeof(header) + sizeof(body))
        if (t == block + 1 + (block->size / sizeof(memory_block_t)))
        {
            block->size += sizeof(memory_block_t) + t->size;
            block->next = t->next;
            if (block->next != NULL)
            {
                block->next->previous = block;
            }
        }
    }
    else // not in front of the first free block
    {
        memory_block_t* iter = root->free_space_list;
        while (block > iter->next && iter->next != NULL)
        {
            iter = iter->next;
        }

        if (iter->next == NULL)
        {
            // case 2.2
            // lies behind the last memory block
            iter->next      = block;
            block->previous = iter;
            block->next     = NULL;

            // pseudocode:
            // if (block == *iter + sizeof(header) + sizeof(body))
            if (block == iter + 1 + (iter->size / sizeof(memory_block_t)))
            {
                iter->size += sizeof(memory_block_t) + block->size;
                iter->next = NULL;
            }
        }
        else
        {
            int merged = 0;
            // case 3
            // lies between two free memory blocks

            // case3.1 iter->next adj to block
            // if (t == *iter + sizeof(header) + sizeof(body))
            if (iter->next ==
                block + 1 + (block->size / sizeof(memory_block_t)))
            {
                merged = 1;
                block->size += sizeof(memory_block_t) + iter->next->size;
                block->next = iter->next->next;
                if (iter->next->next != NULL)
                {
                    block->next->previous = block;
                } // if we merged the last item
                iter->next      = block;
                block->previous = iter;
            }
            // case 3.2 block directly behind iter
            //
            // pseudocode:
            // if (block == *iter + sizeof(header) + sizeof(body))
            if (block == iter + 1 + (iter->size / sizeof(memory_block_t)))
            {
                if (merged == 1)
                { // if our block has been merged in case 3.1
                    iter->next = block->next;
                    if (iter->next != NULL)
                    { // it is possible that we merged the last two items of the
                      // list
                        iter->next->previous = iter;
                    }
                    iter->size += sizeof(memory_block_t) + block->size;
                }
                else
                {
                    merged = 1;
                    iter->size += sizeof(memory_block_t) + block->size;
                    block->previous = iter;
                    block->next     = iter->next;
                }
            }

            // case nothing was merged
            if (merged == 0)
            {
                iter->next->previous = block;
                block->next          = iter->next;
                iter->next           = block;
                block->previous      = iter;
            }
        }
    }

    snprintf(checkpoint_name, 256, "%03d_my_free", checkpoint_counter++);
    checkpoint(checkpoint_name);
}
