#include "client.h"
#include <arpa/inet.h>
#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

pthread_t reading_thread;
int client_socket;

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

// wobei doch, alles gut glaube ich
// Add your global variables here

void connect_to_server()
{
    int sd = socket(AF_INET, SOCK_STREAM, 0);
    if (sd == -1)
    {
        exit_client(errno);
    }
    client_socket = sd;

    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_port   = htons(get_port_number());

    if (inet_pton(AF_INET, "127.0.0.1", &address.sin_addr) != 1)
    {
        exit_client(errno);
    }

    if (connect(client_socket, (struct sockaddr*)&address, sizeof(address)) ==
        -1)
    {
        exit_client(errno);
    }
}

//-------------------------------------------------------------

void handshake()
{
    // buffer for message
    char buf[MAX_MESSAGE_LENGTH] = {0};

    if (send(client_socket, "Hello, I'm the client",
             strlen("Hello, I'm the client"), 0) == -1)
    {
        exit_client(errno);
    }

    if (recv(client_socket, buf, MAX_MESSAGE_LENGTH, 0) == -1)
    {
        exit_client(errno);
    }

    printf("%s\n", buf);
}

//-------------------------------------------------------------

void send_message()
{
    while (1)
    {
        // buffer for user's message
        char buf[MAX_USER_INPUT] = {0};

        char* msg = prompt_user_input((char*)buf, MAX_USER_INPUT);

        if (strlen(msg) == 0 || strcmp(msg, "\n") == 0)
        {
            get_request();
        }
        else
        {
            set_request(msg);
        }
    }
}

//-------------------------------------------------------------

void set_request(char* message)
{
    pthread_mutex_lock(&lock);

    char s_buf[MAX_USER_INPUT + 2] = "s:";
    char* s_message                = strcat(s_buf, message);
    if (send(client_socket, s_message, strlen(s_message), 0) == -1)
    {
        exit_client(errno);
    }

    char rec[MAX_MESSAGE_LENGTH] = "";
    if (recv(client_socket, &rec, MAX_MESSAGE_LENGTH, 0) == -1)
    {
        exit_client(errno);
    }

    if (strncmp((const char*)&rec, "r:nack", 6) == 0)
    {
        prompt_error();
    }
    else if (strncmp((const char*)&rec, "r:nack", 6) != 0 &&
             strncmp((const char*)&rec, "r:ack", 5) != 0)
    {
        fprintf(stderr, "%s\n", rec);
        exit_client(EIO);
    }

    pthread_mutex_unlock(&lock);
}

//-------------------------------------------------------------

void get_request()
{
    pthread_mutex_lock(&lock);

    char* g_message = "g:";
    if (send(client_socket, g_message, strlen(g_message), 0) == -1)
    {
        exit_client(errno);
    }

    char rec[MAX_MESSAGE_LENGTH] = "";
    if (recv(client_socket, &rec, MAX_MESSAGE_LENGTH, 0) == -1)
    {
        exit_client(errno);
    }

    if (strncmp((const char*)&rec, "r:nack", 6) != 0)
    {
        print_reply((char*)&rec);
    }

    pthread_mutex_unlock(&lock);
}

//-------------------------------------------------------------

void* read_continously(void* unused)
{
    (void)unused; // Mark variable as used for the compiler :-)

    while (1)
    {
        sleep(READING_INTERVAL);
        get_request();
    }

    // this method should not return so dont care about return value
    return NULL;
}

//-------------------------------------------------------------

void start_reader_thread()
{
    if (pthread_create(&reading_thread, NULL, read_continously, NULL) != 0)
    {
        exit_client(errno);
    }
}
