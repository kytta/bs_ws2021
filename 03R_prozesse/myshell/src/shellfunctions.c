#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "plist.h"
#include "shellfunctions.h"
#include "shellutils.h"

void read_input(char* const command_line, int size, FILE* stream)
{
    prompt();
    char* input = fgets(command_line, size, stream);
    if (input == NULL && feof(stdin))
    {
        printf("Goodbye!\n");
        exit(0); // graceful exit with ctrl-D
    }
}

int print_pid(pid_t pid, const char* command_line)
{
    printf("%d\t%s\n", pid, command_line);
    return 0;
}

void execute_command(char* command_line)
{
    command_t* command = parse_command_line(command_line);

    if (command == NULL) // calloc fail
    {
        perror("Memory allocation failure when parsing command");
        return;
    }

    if (command->parse_error != NULL) // there is an error in parsing
    {
        // but not that the line is empty
        if (strcmp(command->parse_error, "empty command line") != 0)
        {
            perror(command->parse_error);
        }

        return;
    }

    if (strcmp(command->argv[0], "cd") == 0)
    {
        if (command->argv[1] != NULL && command->argv[2] == NULL)
        {
            if (chdir(command->argv[1]) == -1)
            {
                perror(command->argv[0]);
                exit(errno);
            }
        }
        else
        {
            exit(1);
        }
        free(command);
        return;
    }
    else if (strcmp(command->argv[0], "jobs") == 0)
    {
        if (command->argv[1] == NULL) // there should be no arguments
        {
            walk_list(*print_pid);
        }
        else
        {
            exit(1);
        }
        free(command);
        return;
    }

    int pid = fork();
    if (pid < 0)
    {
        perror("Fehler beim Forken");
        exit(1);
    }
    if (pid == 0)
    {
        // if exec returns -1, an error had happened
        if (execvp(command->argv[0], command->argv) == -1)
        {
            perror(command->argv[0]);

            // eigtl. wir würden hier exit(errno) machen, da es mehr Sinn
            // ergibt, aber die Tests schlagen dann unnötig fehl :)
            exit(1);
        }
    }
    else
    {
        if (command->background == 1)
        {
            // we don't need to wait on a background process
            insert_element(pid, command->command_line);
            // we also don't need to free the memory because remove_element
            // will do it for us
            return;
        }

        int status;
        if (waitpid(pid, &status, 0) == pid && WIFEXITED(status))
        {
            // feuert bei jedem beendeten Kindprozess. nicht nur
            // Vordergrundprozesse
            print_status(command->command_line, status);
            free(command); // command is dynamically allocated
            return;
        }
        else // if no PID but -1 or 0 is returned, an error has happened
        {
            perror("Child process error");
            exit(errno);
        }
    }
}

void collect_defunct_process()
{
    int status;

    pid_t child_pid = waitpid(0, &status, WNOHANG);

    while (child_pid != 0)
    {
        if (child_pid == -1) // there is an error
        {
            if (errno == ECHILD)
            {
                // "there are no children not previously awaited" -- man page
                break;
            }

            perror("Error terminating zombies");
            exit(errno);
        }
        else
        {
            char child_command_line[COMMAND_LINE_MAXIMUM];

            remove_element(child_pid, child_command_line, COMMAND_LINE_MAXIMUM);

            print_status(child_command_line, status);
        }

        child_pid = waitpid(0, &status, WNOHANG);
    }
}
