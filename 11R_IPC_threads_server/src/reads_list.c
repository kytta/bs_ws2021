#include "reads_list.h"
#include "pthread.h"
#include <errno.h>
#include <fcntl.h> /* For O_* constants */
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h> /* For mode constants */
#include <sys/types.h>
#include <unistd.h>

typedef struct reads_list_element
{
    unsigned int client_number;
    struct reads_list_element* next;
    struct reads_list_element* previous;
    sem_t reader_semaphore;

} reads_list_element_t;

reads_list_element_t* head = NULL;

// Lock to block concurrent writing on reads_list
pthread_rwlock_t reads_list_lock = PTHREAD_RWLOCK_INITIALIZER;

//-----------------------------------------------------------------------------

int reads_list_insert_element(unsigned int client_number)
{
    // create new element
    reads_list_element_t* new_element = malloc(sizeof(reads_list_element_t));
    if (new_element == NULL)
    {
        perror("reads_list_insert_element: malloc");
        exit(EXIT_FAILURE);
    }

    new_element->client_number = client_number;
    sem_init(&(new_element->reader_semaphore), 0, 0);

    // insert element into list
    pthread_rwlock_wrlock(&reads_list_lock);
    if (head == NULL)
    {
        new_element->next     = NULL;
        new_element->previous = NULL;
        head                  = new_element;

        pthread_rwlock_unlock(&reads_list_lock);
        return 0;
    }

    reads_list_element_t* temporary = head;
    while (temporary->next != NULL)
    {
        temporary = temporary->next;
    }

    new_element->next     = NULL;
    new_element->previous = temporary;
    temporary->next       = new_element;

    pthread_rwlock_unlock(&reads_list_lock);
    return 0;
}

//-----------------------------------------------------------------------------

sem_t* reads_list_get_reader_semaphore(unsigned int client_number)
{

    pthread_rwlock_rdlock(&reads_list_lock);
    reads_list_element_t* temporary = head;
    pthread_rwlock_unlock(&reads_list_lock);

    while (temporary != NULL)
    {
        if (temporary->client_number == client_number)
        {
            return &(temporary->reader_semaphore);
        }

        temporary = temporary->next;
    }

    return NULL;
}

//-----------------------------------------------------------------------------

void reads_list_increment_all()
{
    pthread_rwlock_rdlock(&reads_list_lock);
    reads_list_element_t* temporary = head;

    while (temporary != NULL)
    {
        sem_post(&(temporary->reader_semaphore));
        temporary = temporary->next;
    }
    pthread_rwlock_unlock(&reads_list_lock);
}

//-----------------------------------------------------------------------------

void reads_list_decrement(unsigned int client_number)
{
    pthread_rwlock_rdlock(&reads_list_lock);
    reads_list_element_t* temporary = head;

    while (temporary != NULL)
    {
        if (temporary->client_number == client_number)
        {
            sem_wait(&(temporary->reader_semaphore));
            break;
        }

        temporary = temporary->next;
    }
    pthread_rwlock_unlock(&reads_list_lock);
}

//-----------------------------------------------------------------------------

int reads_list_remove_reader(unsigned int client_number)
{
    // find element to remove
    pthread_rwlock_rdlock(&reads_list_lock);
    reads_list_element_t* temporary = head;

    while (temporary != NULL && temporary->client_number != client_number)
    {
        temporary = temporary->next;
    }

    if (temporary == NULL)
    {
        return -1;
    }
    pthread_rwlock_unlock(&reads_list_lock);

    pthread_rwlock_wrlock(&reads_list_lock);
    // bend pointers around element
    if (temporary->previous != NULL)
    {
        temporary->previous->next = temporary->next;
    }
    if (temporary->next != NULL)
    {
        temporary->next->previous = temporary->previous;
    }
    if (temporary == head)
    {
        head = temporary->next;
    }
    pthread_rwlock_unlock(&reads_list_lock);

    // finally delete element
    free(temporary);
    return 0;
}

//-----------------------------------------------------------------------------

int reads_list_get_reads(unsigned int client_number)
{
    int buffer = 0;

    sem_t* reader_semaphore = reads_list_get_reader_semaphore(client_number);

    if (reader_semaphore == NULL)
    {
        return 0;
    }

    sem_getvalue(reader_semaphore, &buffer);
    return buffer;
}
