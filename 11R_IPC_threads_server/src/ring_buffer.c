#include "../include/ring_buffer.h"
#include "../include/reads_list.h"
#include "../include/server.h"

#include "pthread.h"
#include "semaphore.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "time.h"

typedef struct ring_buffer_element
{
    pthread_mutex_t to_read_mutex;
    char text[MAX_MESSAGE_LENGTH];
    int to_read;
} ring_buffer_element_t;

static ring_buffer_element_t ring_buffer[RINGBUFFER_SIZE];

unsigned int mutexes_initted          = 0;
pthread_mutex_t mutexes_initted_mutex = PTHREAD_MUTEX_INITIALIZER;

unsigned int current_writer          = 0;
pthread_mutex_t current_writer_mutex = PTHREAD_MUTEX_INITIALIZER;

unsigned int clients          = 0; // aka number_of_readers
pthread_mutex_t clients_mutex = PTHREAD_MUTEX_INITIALIZER;

pthread_cond_t can_write        = PTHREAD_COND_INITIALIZER;
pthread_mutex_t can_write_mutex = PTHREAD_MUTEX_INITIALIZER;

//-----------------------------------------------------------------------------

int ringbuffer_write(char* text)
{
    pthread_mutex_lock(&current_writer_mutex);

    // Check if thread can write a new element, and synchronization will be
    // needed
    pthread_mutex_lock(
        &(ring_buffer[current_writer % RINGBUFFER_SIZE].to_read_mutex));
    int to_read = ring_buffer[current_writer % RINGBUFFER_SIZE].to_read;
    pthread_mutex_unlock(
        &(ring_buffer[current_writer % RINGBUFFER_SIZE].to_read_mutex));

    if (to_read > 0)
    {
        struct timespec timeout;

        if (clock_gettime(CLOCK_REALTIME, &timeout) == -1)
        {
            perror("ringbuffer_write: clock_gettime");
            return -1;
        }

        timeout.tv_sec += 1;

        // buffer full -- wait until there is place
        pthread_mutex_lock(&can_write_mutex);
        if (pthread_cond_timedwait(&can_write, &can_write_mutex, &timeout) != 0)
        {
            pthread_mutex_unlock(&can_write_mutex);
            pthread_mutex_unlock(&current_writer_mutex);
            return -1;
        }
        else
        {
            pthread_mutex_unlock(&can_write_mutex);
        }
    }

    pthread_mutex_lock(
        &(ring_buffer[current_writer % RINGBUFFER_SIZE].to_read_mutex));
    // Write element
    strcpy(ring_buffer[current_writer % RINGBUFFER_SIZE].text, text);

    pthread_mutex_lock(&clients_mutex);
    ring_buffer[current_writer % RINGBUFFER_SIZE].to_read = clients;
    pthread_mutex_unlock(&clients_mutex);
    pthread_mutex_unlock(
        &(ring_buffer[current_writer % RINGBUFFER_SIZE].to_read_mutex));

    reads_list_increment_all();

    current_writer++;

    pthread_mutex_unlock(&current_writer_mutex);

    return 0;
}

//-----------------------------------------------------------------------------

void ringbuffer_read(int* current_reader, char* buffer,
                     unsigned int client_number)
{

    int reader = *current_reader;

    if (reads_list_get_reads(client_number) == 0)
    {
        // nothing to read
        strcpy(buffer, "nack");
        return;
    }

    pthread_mutex_lock(&(ring_buffer[reader % RINGBUFFER_SIZE].to_read_mutex));
    ring_buffer[reader % RINGBUFFER_SIZE].to_read--;
    int left_to_read = ring_buffer[reader % RINGBUFFER_SIZE].to_read;
    pthread_mutex_unlock(&ring_buffer[reader % RINGBUFFER_SIZE].to_read_mutex);

    reads_list_decrement(client_number);

    // Read Element
    pthread_mutex_lock(&(ring_buffer[reader % RINGBUFFER_SIZE].to_read_mutex));
    strcpy(buffer, (const char*)ring_buffer[reader % RINGBUFFER_SIZE].text);
    pthread_mutex_unlock(&ring_buffer[reader % RINGBUFFER_SIZE].to_read_mutex);

    pthread_mutex_lock(&can_write_mutex);
    if (left_to_read == 0)
    {
        // notify writer
        pthread_cond_signal(&can_write);
    }
    pthread_mutex_unlock(&can_write_mutex);

    // Update reader count
    (*current_reader)++;

    return;
}

//-----------------------------------------------------------------------------

int ringbuffer_add_reader(unsigned int client_number)
{
    ringbuffer_init_mutexes();

    // HINT: synchronization is needed in this function

    if (reads_list_insert_element(client_number) != 0)
    {
        exit(EXIT_FAILURE);
    }

    pthread_mutex_lock(&clients_mutex);
    clients++;
    pthread_mutex_unlock(&clients_mutex);

    pthread_mutex_lock(&current_writer_mutex);
    int new_reader = current_writer;
    pthread_mutex_unlock(&current_writer_mutex);

    return new_reader;
}

//-----------------------------------------------------------------------------

void ringbuffer_remove_reader(int* current_reader, unsigned int client_number)
{
    // HINT: synchronization is needed in this function

    int reads = reads_list_get_reads(client_number);

    // perform all unfinished reads for the disconnected client
    while (reads != 0)
    {
        char buffer[MAX_MESSAGE_LENGTH];
        ringbuffer_read(current_reader, buffer, client_number);
        reads = reads_list_get_reads(client_number);
    }

    reads_list_remove_reader(client_number);

    pthread_mutex_lock(&clients_mutex);
    clients--;
    pthread_mutex_unlock(&clients_mutex);

    return;
}

void ringbuffer_init_mutexes()
{
    pthread_mutex_lock(&mutexes_initted_mutex);
    if (mutexes_initted == 0)
    {
        // this happens only once

        int i;
        for (i = 0; i < RINGBUFFER_SIZE; i++)
        {
            pthread_mutex_init(&ring_buffer[i].to_read_mutex, NULL);
        }
        mutexes_initted = 1;
    }
    pthread_mutex_unlock(&mutexes_initted_mutex);
}
