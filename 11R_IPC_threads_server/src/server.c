#include "../include/server.h"

#include "ring_buffer.h"
#include "semaphore.h"
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h> /* For O_* constants */
#include <netinet/ip.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h> /* For mode constants */
#include <sys/types.h>
#include <unistd.h>

uint16_t get_port_number()
{
    u_int16_t port       = 0;
    const char* username = getenv("USER");
    if (username == NULL)
    {
        return 31026;
    }
    char c;
    while ((c = *username) != '\0')
    {
        port = (port << 1) + port + (u_int16_t)c;
        username++;
    }
    return 31026 + (port % 4096);
}

unsigned int client_number_count = 0;
struct sockaddr_in address;
int addrlen = sizeof(address);

sem_t client_sock_sem;

pthread_mutex_t client_number_mutex = PTHREAD_MUTEX_INITIALIZER;

int initialize_server()
{
    int waiting_socket = socket(AF_INET, SOCK_STREAM, 0);

    if (waiting_socket == -1)
    {
        perror("initialize_server: socket");
        exit(errno);
    }

    address.sin_family      = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port        = htons(SERVER_PORT);

    if (bind(waiting_socket, (struct sockaddr*)&address, addrlen) < 0)
    {
        perror("initialize_server: bind");
        exit(errno);
    }

    return waiting_socket;
}

//-----------------------------------------------------------------------------

int handshake(int file_descriptor)
{
    // Read message from client
    char message_buffer[MAX_MESSAGE_LENGTH];
    ssize_t message_lenght = 0;
    if ((message_lenght =
             read(file_descriptor, message_buffer, MAX_MESSAGE_LENGTH - 1)) < 1)
    {
        return -1;
    }

    // Enforce NULL Terminated string
    message_buffer[MAX_MESSAGE_LENGTH - 1] = '\0';
    printf("Handshake: %s\n", message_buffer);

    // Reply message to client
    const char server_message[] = SERVER_HANDSHAKE_MSG;
    if (write(file_descriptor, server_message, sizeof(server_message)) < 0)
    {
        return -1;
    }

    return 0;
}

//-----------------------------------------------------------------------------

void* handle_connection(void* socket)
{
    int file_descriptor = *(int*)socket;
    free(socket); // can also be freed when thread exits

    // get client number
    pthread_mutex_lock(&client_number_mutex);
    unsigned int client_number = client_number_count;
    client_number_count++;
    pthread_mutex_unlock(&client_number_mutex);

    printf("adding reader no %d\n", client_number);

    // get reader number for thread
    int thread_reader = ringbuffer_add_reader(client_number);

    // handshake
    if (handshake(file_descriptor) < 0)
    {
        printf("handshake failed \n");
        ringbuffer_remove_reader(&thread_reader, file_descriptor);
        close(file_descriptor);
        return NULL;
    }

    void* buffer = malloc(sizeof(char) * (MAX_MESSAGE_LENGTH));
    if (buffer == NULL)
    {
        perror("malloc");
        ringbuffer_remove_reader(&thread_reader, file_descriptor);
        close(file_descriptor);
        return NULL;
    }
    // main loop for each thread to continuesly read from file descriptor and
    // handle input

    while (1)
    {
        // clean the buffer
        memset(buffer, 0, MAX_MESSAGE_LENGTH);

        int number_of_read_bytes =
            read(file_descriptor, buffer, MAX_MESSAGE_LENGTH - 1);
        char* message                   = (char*)buffer;
        message[MAX_MESSAGE_LENGTH - 1] = '\0';

        // error handling for read
        if (number_of_read_bytes < 0)
        {
            ringbuffer_remove_reader(&thread_reader, client_number);
            perror("read");
            if (close(file_descriptor) < 0)
            {
                perror("close");
            }
            break;
        }
        else if (number_of_read_bytes == 0)
        {
            ringbuffer_remove_reader(&thread_reader, client_number);
            if (close(file_descriptor) < 0)
            {
                perror("close");
            }
            printf("closing connection\n");
            break;
        }

        // handle clients input
        if (handle_input(client_number, message, file_descriptor,
                         &thread_reader) != 0)
        {
            ringbuffer_remove_reader(&thread_reader, client_number);
            close(file_descriptor);
            break;
        }
    }
    free(buffer);
    return NULL;
}

//-----------------------------------------------------------------------------

int handle_input(int client_number, char* input, int socket,
                 int* current_reader_pointer)
{

    const char* error_message_1       = "r:invalid input: short message";
    const char* error_message_2       = "r:invalid input: unknown message type";
    const char* write_error_message   = "r:nack";
    const char* write_success_message = "r:ack";

    // check message length
    if (sizeof(input) < 2 * sizeof(char))
    {

        if (write(socket, error_message_1, strlen(error_message_1) + 1) < 0)
        {
            perror("write");
            return -1;
        }
        return 0;
    }

    // check first two chars of message
    char control_character = input[0];
    char delimiter         = input[1];
    if (delimiter != ':' ||
        !(control_character == 'g' || control_character == 's'))
    {
        printf("invalid input\n");
        if (write(socket, error_message_2, strlen(error_message_2) + 1) < 0)
        {
            perror("write");
            return -1;
        }
        return 0;
    }

    char* message = ++input;
    message++;

    // handle GET request
    if (control_character == 'g')
    {
        char buffer[MAX_MESSAGE_LENGTH - 2];

        ringbuffer_read(current_reader_pointer, buffer, client_number);

        printf("client %d read: %s length %zu\n", client_number, buffer,
               strlen(buffer));

        char message[MAX_MESSAGE_LENGTH] = "r:";

        strcat(message, buffer);

        if (write(socket, message, strlen(message) + 1) < 0)
        {
            perror("write");
            return (-1);
        }
        // handle set request
    }
    // handle SET request
    else if (control_character == 's')
    {

        // write in ringbuffer
        int write_ack = ringbuffer_write(message);

        // write failed
        if (write_ack != 0)
        {
            if (write(socket, write_error_message,
                      strlen(write_error_message) + 1) < 0)
            {
                perror("write");
                return -1;
            }
            printf("client %d write failed\n", client_number);
        }
        // write success
        else
        {
            if (write(socket, write_success_message,
                      strlen(write_success_message) + 1) < 0)
            {
                perror("write");
                return -1;
            }
            printf("client %d write: %s length %zu\n", client_number, message,
                   strlen(message));
        }
    }
    else
    {
        printf("an unknown error occured\n");
        return (-1);
    }

    return 0;
}

//-----------------------------------------------------------------------------

void accept_connections(int socket_number)
{

    if (listen(socket_number, MAX_SOCKET_QUEUE) == -1)
    {
        perror("accept_connections: listen");
        exit(errno);
    }

    while (1)
    {
        if (sem_init(&client_sock_sem, 0, 1) == -1)
        {
            perror("accept_connections: sem_init");
            exit(errno);
        }

        sem_wait(&client_sock_sem);
        int* client_sock = malloc(sizeof(int));

        if (client_sock == NULL)
        {
            perror("accept_connections: malloc");
            exit(errno);
        }

        *client_sock = accept(socket_number, (struct sockaddr*)&address,
                              (socklen_t*)&addrlen);

        if (*client_sock < 0)
        {
            perror("accept_connections: accept");
            exit(errno);
        }

        pthread_t client_thread; //? save in list?
        pthread_attr_t client_thread_attr;

        if ((errno = pthread_attr_init(&client_thread_attr)) != 0)
        {
            perror("accept_connections: pthread_attr_init");
            exit(errno);
        }

        if ((errno = pthread_attr_setdetachstate(&client_thread_attr,
                                                 PTHREAD_CREATE_DETACHED)) != 0)
        {
            perror("accept_connections: pthread_attr_setdetachstate");
            exit(errno);
        }

        if ((errno = pthread_create(&client_thread, &client_thread_attr,
                                    handle_connection, client_sock)) != 0)
        {
            perror("accept_connections: pthread_create");
            exit(errno);
        }

        sem_post(&client_sock_sem);
    }
}
