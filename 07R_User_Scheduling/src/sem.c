#include <stdio.h>

#include "queue.h"
#include "sem.h"
#include "tcb.h"
#include "threads.h"

struct node
{
    TCB* thread;
    struct node* next;
};

Semaphore_t* semaphore_create(int initial_value)
{
    if (initial_value < 0)
    {
        fprintf(stderr, "Semaphor's initial value can't be negative.");
        exit(1);
    }

    Semaphore_t* sem;
    if ((sem = malloc(sizeof(Semaphore_t))) == NULL ||
        (sem->queue = malloc(sizeof(QUEUE))) == NULL)
    {
        perror("semaphore_create");
        exit(1);
    }
    sem->value = initial_value;

    return sem;
}

void semaphore_destroy(Semaphore_t* sem)
{
    free(sem->queue);
    free(sem);
}

void sem_wait(Semaphore_t* sem)
{
    block_sigprof();
    if (sem->value <= 0)
    {
        // no further threads are allowed to enter the critical area

        TCB* current_thread = get_running_thread();
        struct node* cur    = sem->queue->head;
        while (cur != NULL && cur->thread->id != current_thread->id)
        {
            cur = cur->next;
        }

        // if thread not already in Semaphor's queue, enqueue it
        if (cur == NULL)
        {
            queue_enqueue(sem->queue, current_thread);
            sem->value -= 1;
        }

        unblock_sigprof();

        // 1 <=> don't reschedule <=> don't re-enqueue in feedback queue
        threads_yield(1);
    }
    else
    {
        sem->value -= 1;
        unblock_sigprof();
    }
}

void sem_post(Semaphore_t* sem)
{
    block_sigprof();
    if (sem->value < 0)
    {
        if (queue_size(sem->queue) == 0 || sem->queue->head == NULL)
        {
            return;
        }

        TCB* opened_thread = queue_dequeue(sem->queue);

        QUEUE* q = get_feedback_queue();

        int i = 0;
        while (i < opened_thread->feedback_depth)
        {
            q = q->next;
            i += 1;
        }

        if (queue_enqueue(q, opened_thread) != 0)
        {
            perror("sem_post");
            exit(1);
        }

        sem->value += 1;
    }
    unblock_sigprof();
}